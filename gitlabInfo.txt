Git global setup

git config --global user.name "Hijbullah Muhamed Kyrul Amin"
git config --global user.email "hijbullaah@gmail.com"

Create a new repository

git clone https://gitlab.com/Hijbullah/Hijbullah_178293_B69_session6_css_bootstrap.git
cd Hijbullah_178293_B69_session6_css_bootstrap
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/Hijbullah/Hijbullah_178293_B69_session6_css_bootstrap.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://gitlab.com/Hijbullah/Hijbullah_178293_B69_session6_css_bootstrap.git
git push -u origin --all
git push -u origin --tags